# node-exercise
A little exercise using a Star Wars API [https://swapi.dev/](https://swapi.dev/) and [express.js](https://expressjs.com/)

## Goal
* Consume and manipulate API data -done
* Use pagination to get the complete list of all of a resource at once. -done
* Keep the task simple. -done :)
* Sort an array of objects. -done
* Replace object field values with more appropriate data. -done

## To Run My Solution
 run 
 ```properties
 npm install
 ``` 
 and then 
 ```properties
 node app.js
 ```

Dont forget to check out the development branch as well for more commit history! 


