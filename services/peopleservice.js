import axios from 'axios'

async function getAllPeople(sortByFilter) {
    let unsortedPeople = []
    for (let i = 1; i < 10; i++) {
        let page = await axios.get(`https://swapi.dev/api/people?page=${i}`)
        unsortedPeople = [...unsortedPeople, ...page.data.results]
    }

    if (
        sortByFilter != 'height' &&
        sortByFilter != 'mass' &&
        sortByFilter != 'name'
    )
        return unsortedPeople

    if (sortByFilter == 'name')
        return unsortedPeople.sort((current, next) =>
            current[sortByFilter].localeCompare(next[sortByFilter])
        )

    let sortedPeople = unsortedPeople.sort(
        (current, next) => next[sortByFilter] - current[sortByFilter]
    )

    return sortedPeople
}

async function getNamesByURLs(urls) {
    if (urls.length == 0) return []
    let names = []
    await axios.all(
        urls.map(async url => {
            const result = await axios.get(url)
            names.push(result.data['name'])
        })
    )

    return names
}

export default { getAllPeople, getNamesByURLs }
