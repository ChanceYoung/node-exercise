import axios from 'axios'
import peopleService from './peopleservice.js'

async function getAllPlanets() {
    let planets = []
    for (let i = 1; i < 7; i++) {
        let page = await axios.get(`https://swapi.dev/api/planets?page=${i}`)
        planets = [...planets, ...page.data.results]
    }
    for (let i = 0; i < planets.length; i++) {
        const names = await peopleService.getNamesByURLs(
            planets[i]['residents']
        )
        planets[i]['residents'] = names
    }
    return planets
}

export default { getAllPlanets }
