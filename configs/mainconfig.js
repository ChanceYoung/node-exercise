import express from 'express'
import pkg from 'body-parser'
import mainRouter from '../routes/mainrouter.js'
const { urlencoded, json } = pkg
const app = express()
const port = process.env.PORT || 3000

const serverStartup = () => {
    app.use(urlencoded({ extended: true }))
    app.use(json())
    app.use(mainRouter)
    app.listen(port, () => {
        console.log('Custom SWAPI running on port:', port)
    })
}

export default serverStartup
