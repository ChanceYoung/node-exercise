import peopleService from '../services/peopleservice.js'
import planetService from '../services/planetservice.js'

async function getPeople(req, res) {
    try {
        const filter = req.query.sortBy
        const results = await peopleService.getAllPeople(filter)
        if (!results) res.sendStatus(404)
        res.send(results)
    } catch (error) {
        console.error('OOPS, something went wrong: ', error)
        res.sendStatus(500)
    }
}
async function getPlanets(req, res) {
    try {
        const results = await planetService.getAllPlanets()
        if (!results) res.sendStatus(404)
        res.send(results)
    } catch (error) {
        console.error('OOPS, something went wrong: ', error)
        res.sendStatus(500)
    }
}

export default { getPeople, getPlanets }
