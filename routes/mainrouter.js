import { Router } from 'express'
import mainController from '../controllers/maincontroller.js'
const router = new Router()

router.get(
    '/people',
    async (req, res) => await mainController.getPeople(req, res)
)
router.get(
    '/planets',
    async (req, res) => await mainController.getPlanets(req, res)
)

export default router
